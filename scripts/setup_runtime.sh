# input
export WORKSPACE=$HOME
export BUILD_TYPE="Release"

#all variables
export UTOPIA_SRC_DIR=$WORKSPACE/utopia/utopia
export UTOPIA_FE_SRC_DIR=$WORKSPACE/utopia/utopia_fe
export PAR_MOONOLITH_SRC_DIR=$WORKSPACE/par_moonolith/
export MARS_SRC_DIR=$WORKSPACE/mars/
export INSTALL_DIR=$WORKSPACE/installations
export UTOPIA_DIR=$INSTALL_DIR/utopia
export UTOPIA_FE_DIR=$INSTALL_DIR/utopia_fe
export LIBMESH_DIR=$UTOPIA_FE_SRC_DIR/external/libmesh

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$UTOPIA_FE_SRC_DIR/lib/:$UTOPIA_SRC_DIR/external/petsc_debug/lib:$LIBMESH_DIR/lib