source /opt/cray/pe/cdt/19.10/restore_system_defaults.sh >/dev/null 2>&1
module purge >/dev/null 2>&1
module load modules
module load PrgEnv-gnu
module load daint-gpu
module load cray-mpich
module load cray-hdf5-parallel
module load cray-netcdf-hdf5parallel
module load CMake
module load cray-libsci
module load VTK
export CRAYPE_LINK_TYPE=dynamic
export CC=cc
export CXX=CC
export FC=ftn
export F90=ftn
export F77=ftn
CXXFLAGS="$(printf '%s\n' "${CXXFLAGS} -std=c++11" | awk -v RS='[[:space:]]+' '!a[$0]++{printf "%s%s", $0, RT}')"
export CXXFLAGS

export INSTALL_DIR=/apps/daint/UES/anfink/cpu_20200218_Release
export P4EST_DIR=$INSTALL_DIR/p4est
export BOOST_DIR=$INSTALL_DIR/boost
export EIGEN_DIR=$INSTALL_DIR/eigen
export TRILINOS_DIR=$INSTALL_DIR/trilinos
# export PETSC_DIR=$INSTALL_DIR/petsc
# export LIBMESH_DIR=$INSTALL_DIR/libmesh
# export MOOSE_DIR=$INSTALL_DIR/moose
# export METHOD="opt"
# export MOONOLITH_DIR=$INSTALL_DIR/moonolith
# export UTOPIA_DIR=$INSTALL_DIR/utopia
# export LD_LIBRARY_PATH="${P4EST_DIR}/lib:${TRILINOS_DIR}/lib:${PETSC_DIR}/lib:${MOOSE_DIR}/framework:${LIBMESH_DIR}/lib:$LD_LIBRARY_PATH"