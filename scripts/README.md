# Scripts

## Modules

When working on the `hpc` machine at ICS always load the modules with

`source load_hpc_modules.sh`


## Download the necessary software

Donwload the software stack once the first time call this in your home directory

`source downloadsw.sh`

## Compilation

When you want to compile for the first time or re-compile from scratch use

`source compile.sh`

ATTENTION: on the `hpc` cluster compilation has to happen on the compute node. Use first

`salloc --time 04:20:00`

 

You can download and edit the files and only run part of them at your will


