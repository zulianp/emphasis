#!/bin/bash
echo "loading hpc.ics.usi.ch modules"

module purge

module load libnl/3.3.0-gcc-9.2.0-xmnwhyg
module load ucx/1.6.1-gcc-9.2.0-qlqojbw
module load zlib/1.2.11-gcc-9.2.0-35samuw
module load hwloc/1.11.11-gcc-9.2.0-esxiy7f
module load cmake/3.16.1-gcc-9.2.0-w6ff6gf
module load rdma-core/20-gcc-9.2.0-px6zy6l
module load libiconv/1.16-gcc-9.2.0-t4rvikf
module load libxml2/2.9.9-gcc-9.2.0-46dzkwy
module load slurm/15.08.9-gcc-9.2.0-qxlsb3k
module load ncurses/6.1-gcc-9.2.0-ceim77d
module load git/2.13.0
module load numactl/2.0.12-gcc-9.2.0-6yogfwi
module load xz/5.2.4-gcc-9.2.0-mvfzuq4
module load libpciaccess/0.13.5-gcc-9.2.0-72eie6v
module load openmpi/3.1.4-gcc-9.2.0-mcc43h7
module load openssl/1.1.1d-gcc-9.2.0-jkywy6c


UCX_WARN_UNUSED_ENV_VARS=n

# Autocomplete 
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
