#!/bin/bash
# install utopia
git clone --recurse-submodules https://bitbucket.org/zulianp/utopia.git
cd utopia
git checkout edsl_refactor
cd -

# install moonolith
git clone https://zulianp@bitbucket.org/zulianp/par_moonolith.git

# install mars
git clone https://zulianp@bitbucket.org/zulianp/mars.git
