#!/bin/bash

# input
export WORKSPACE=$HOME
export BUILD_TYPE="Release"

#all variables
export UTOPIA_SRC_DIR=$WORKSPACE/utopia/utopia
export UTOPIA_FE_SRC_DIR=$WORKSPACE/utopia/utopia_fe
export PAR_MOONOLITH_SRC_DIR=$WORKSPACE/par_moonolith/
export MARS_SRC_DIR=$WORKSPACE/mars/
export INSTALL_DIR=$WORKSPACE/installations
export UTOPIA_DIR=$INSTALL_DIR/utopia
export UTOPIA_FE_DIR=$INSTALL_DIR/utopia_fe
export MOONOLITH_DIR=$INSTALL_DIR/par_moonolith

export DEBUG_BUILD_TYPE=Debug
export RELEASE_BUILD_TYPE=Release

# clean-up harmful env variables
unset PETSC_DIR
unset LIBMESH_DIR

# actual compilation
if [ "$BUILD_TYPE" = "$DEBUG_BUILD_TYPE" ]; then
    cd $UTOPIA_SRC_DIR && git pull
    cd $UTOPIA_SRC_DIR && rm -rf build
    cd $UTOPIA_SRC_DIR && mkdir build && cd build  \
                       && cmake .. -DINSTALL_PETSC_DEBUG=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$UTOPIA_DIR -DENABLE_CXX14_FEATURES=ON \
                       && make petsc_debug && cmake .. -DPETSC_DIR=../external/petsc_debug && make -j10 && make -j10 complete && make install

    # make petsc visible to libmesh
    export PETSC_DIR=$UTOPIA_SRC_DIR/external/petsc_debug
else
    cd $UTOPIA_SRC_DIR && git pull
    cd $UTOPIA_SRC_DIR && rm -rf build
    cd $UTOPIA_SRC_DIR && mkdir build && cd build  \
                       && cmake .. -DINSTALL_PETSC=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$UTOPIA_DIR -DENABLE_CXX14_FEATURES=ON \
                       && make petsc && cmake .. -DPETSC_DIR=../external/petsc && make -j10 && make -j10 complete && make install \
                       && export PETSC_DIR=$UTOPIA_SRC_DIR/external/petsc

    # make petsc visible to libmesh

fi

cd $PAR_MOONOLITH_SRC_DIR && rm -rf build
cd $PAR_MOONOLITH_SRC_DIR && mkdir build && cd build  \
                   && cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$MOONOLITH_DIR \
                   && make -j10 && make -j10 pm_test && make install



cd $UTOPIA_FE_SRC_DIR && rm -rf build
cd $UTOPIA_FE_SRC_DIR && mkdir build && cd build  \
                   && cmake .. -DINSTALL_LIBMESH=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$UTOPIA_FE_DIR -DENABLE_CXX14_FEATURES=ON \
                   && make libmesh && cmake .. -DLIBMESH_DIR=../external/libmesh && make -j10 && make -j10 complete && make install


cd $MARS_SRC_DIR && rm -rf build
cd $MARS_SRC_DIR && mkdir build && cd build  \
                 && cmake .. -DTRY_WITH_MOONOLITH=ON -DMOONOLITH_DIR=$MOONOLITH_DIR && make -j10


